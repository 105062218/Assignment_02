var playState = {
    preload: function() {}, // The proload state does nothing now.

    create: function() {
    // Removed background color, physics system, and roundPixels.
    // replace 'var score = 0' by global score variable.
    game.global.score = 0;
    game.global.health = 10;
    // The following part is the same as in previous lecture.

    this.cursor = game.input.keyboard.createCursorKeys();


    //set up player
    this.player = game.add.sprite(game.width/2, 30, 'player');
    this.player.anchor.setTo(0.5,0);
    game.physics.arcade.enable(this.player);
    //this.player.body.gravity.y = 500;
    this.player.animations.add('left', [0, 1, 2, 3], 8);
    this.player.animations.add('right', [9, 10, 11, 12], 8);
    this.player.animations.add('flyleft', [18, 19, 20, 21], 12);
    this.player.animations.add('flyright', [27, 28, 29, 30], 12);
    this.player.animations.add('fly', [36, 37, 38, 39], 12);

    //set up walls
    this.leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(this.leftWall);
    this.leftWall.body.immovable = true;

    this.rightWall = game.add.sprite(383, 0, 'wall');
    game.physics.arcade.enable(this.rightWall);
    this.rightWall.body.immovable = true;

    this.ceiling = game.add.sprite(0, 0, 'ceiling');
    game.physics.arcade.enable(this.ceiling);
    this.ceiling.body.immovable = true;

    this.platforms = game.add.group();
    game.physics.arcade.enable(this.platforms);
    this.platforms.enableBody = true;
    game.time.events.loop(800, this.addPlatform, this);
    //this.platforms.setAll('body.immovable', true);
    
    this.healthLabel = game.add.text(30, 20, 'health: 10', { font: '25px Arial', fill: '#ffffff' });
    this.scoreLabel = game.add.text(250, 20, 'score: 0', { font: '25px Arial', fill: '#ffffff' });

    game.time.events.add(Phaser.Timer.SECOND, function(){this.player.body.gravity.y = 500;}, this);


    this.healSound = game.add.audio('health');
    this.hurtSound = game.add.audio('hurt');
    this.hurtSound.volume = 0.7;
    this.jumpSound = game.add.audio('jump');
    this.turnSound = game.add.audio('turn');
    this.turnSound.volume = 0.5;

    this.BGSound = game.add.audio('BGsong');
    this.BGSound.loop = true;
    this.BGSound.volume = 0.25;
    this.BGSound.play();

    

    },

    update: function() {
        game.physics.arcade.collide(this.player, this.leftWall);
        game.physics.arcade.collide(this.player, this.rightWall);
        game.physics.arcade.collide(this.player, this.ceiling, this.touchCeiling, null, this);
        game.physics.arcade.collide(this.player, this.platforms, this.platformEffect, null, this);
        game.physics.arcade.collide(this.ceiling, this.platforms, this.killPlatform, null, this);
        game.physics.arcade.collide(this.player, this.heart, this.addHealth, null, this);
        //game.physics.arcade.collide(this.player, this.bubble, this.float, null, this);

        this.movePlayer();


        if(!this.player.inWorld) this.playerDie();
        if(game.global.health<=0) this.playerDie();
        if(this.player.body.y>20) this.player.touchDown = null;
        

        var spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        spaceKey.onDown.add(this.pause, this);


        var RKey = game.input.keyboard.addKey(Phaser.Keyboard.R);
        RKey.onDown.add(this.returntoMenu, this);


    }, // No changes

    pause: function(){
        if(game.paused) {
            this.BGSound.resume();
            this.pauseMenu.destroy();
            game.paused = false;
        }
        else {
            game.paused = true;
            this.BGSound.pause();
            this.pauseMenu = game.add.sprite(game.width/2, game.height/2, 'pause');
            this.pauseMenu.anchor.setTo(0.5,0.5);
        }

    },

    returntoMenu: function(){
        if(game.paused) game.paused = false;
        this.BGSound.stop();
        game.state.start('menu');
        
    },

    movePlayer: function() {
        if (this.cursor.left.isDown){
            this.player.body.velocity.x = -250;
        }
        else if (this.cursor.right.isDown){
            this.player.body.velocity.x = 250;
        }// If neither the right or left arrow key is pressed
        else { 
        // Stop the player
            this.player.body.velocity.x = 0;
        }
        
        this.playerAnimation(this.player);


    }, // No changes

    playerAnimation: function(player){
        var vx = player.body.velocity.x;
        var vy = player.body.velocity.y+100;
        if(vx<0&&vy>0) player.animations.play('flyleft');
        if(vx>0&&vy>0) player.animations.play('flyright');
        if(vx<0&&vy==0) player.animations.play('left');
        if(vx>0&&vy==0) player.animations.play('right');
        if(vx==0&&vy!=0) player.animations.play('fly');
        if(vx==0&&vy==0) player.frame = 8;
    },

    takeCoin: function(player, coin) {
        // Use the new score variable game.global.score += 5;
        // Use the new score variable
        game.global.score += 5;
        this.scoreLabel.text = 'score: ' + game.global.score;
        // Then no changes
    },

    addHealth: function(player, heart){
        game.global.health += 1;
        this.healthLabel.text = 'health: ' + game.global.health;
        heart.kill();
        this.healSound.play();
    },

    /*float: function(player, bubble){
        bubble.kill();
        player.body.gravity.y = 0;
        player.body.velocity.y = 0;
        player.body.checkCollision.down = false;
        //player.body.velocity.y = 100;
        game.time.events.add(Phaser.Timer.SECOND*5, function(){player.body.gravity.y = 500;player.body.checkCollision.down = true;}, this);
    },*/

    updateCoinPosition: function() {}, // No changes

    playerDie: function() {
        // When the player dies, go to the menu
        game.state.start('exit');
        this.BGSound.stop();
        
    },

    killPlatform: function(ceiling, platform){
        platform.kill();
    },


    touchCeiling: function(player, ceiling){
        if(player.touchDown !== ceiling) {
            game.global.health -= 1;
            player.touchDown = ceiling;
            game.camera.flash(0xff0000, 100);
            this.hurtSound.play();
        }
        if(player.body.velocity.y<0) player.body.velocity.y = 0;
        if(player.body.y<20) player.reset(player.body.x+20, 20);
        this.healthLabel.text = 'health: '+ game.global.health;
    },

    platformEffect: function(player, platform){
        if(platform.key == 'fake') {
            if(player.touchOn !== platform) {
                platform.animations.play('turn');
                setTimeout(function() {
                    platform.body.checkCollision.up = false;
                }, 100);
                player.touchOn = platform;
                this.turnSound.play();
            }
        }
        else if(platform.key == 'conveyor_left') player.body.x -= 2;
        else if(platform.key == 'conveyor_right') player.body.x += 2;
        else if(platform.key == 'nails') {
            if (player.touchOn !== platform) {
                game.global.health -= 1;
                this.healthLabel.text = 'health: '+ game.global.health;
                player.touchOn = platform;
                game.camera.flash(0xff0000, 100);
                this.hurtSound.play();
            }
        }
        else if(platform.key == 'trampoline'){
            platform.animations.play('jump');
            player.body.velocity.y = -350;
            this.jumpSound.play();
        }
    },
    
    addPlatform: function() {
            // Get the first dead enemy of the group
            var x = game.rnd.between(68, 332);
            var rand = game.rnd.between(1, 10);
            var type;
            var platform;
            if(rand <= 5 && rand >= 1) platform = game.add.sprite(x, 400, 'normal', 0, this.platforms);
            else if(rand==6) platform = game.add.sprite(x, 400, 'nails', 0, this.platforms);
            else if(rand==7) {
                //type = 'conveyor_left';
                platform = game.add.sprite(x, 400, 'conveyor_left', 0, this.platforms)
                platform.animations.add('scroll',[0, 1, 2, 3], 16, true );
                platform.play('scroll');
            }
            else if(rand==8){
                platform = game.add.sprite(x, 400, 'conveyor_right', 0, this.platforms)
                platform.animations.add('scroll',[0, 1, 2, 3], 16, true );
                platform.play('scroll');
            }
            else if(rand==9) {
                platform = game.add.sprite(x, 400, 'fake', 0, this.platforms)
                platform.animations.add('turn',[0, 1, 2, 3, 4, 5, 0], 14);
            }
            else {
                platform = game.add.sprite(x, 400, 'trampoline', 0, this.platforms)
                platform.animations.add('jump',[4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
                platform.frame = 3;
            };
            //var platform = game.add.sprite(x, 400, type, 0, this.platforms);
            // If there isn't any dead enemy, do nothing 
            /* Initialize enemy */ 
            // Set the anchor point centered at the bottom
            platform.anchor.setTo(0.5, 1); 
            // Put the enemy above the top hole
            game.physics.arcade.enable(platform);
            platform.body.velocity.y = -100;
            platform.body.immovable = true;
            platform.body.checkCollision.down = false;
            platform.body.checkCollision.left = false;
            platform.body.checkCollision.right = false;

            game.global.score += 1;
            this.scoreLabel.text = 'score: '+ game.global.score;

            var tool = game.rnd.between(0, 1);
            if(game.global.score%20==0){
                if(rand != 6 && rand != 9){
                    this.heart = game.add.sprite(x, 390, 'heart');
                    this.heart.anchor.setTo(0.5, 1);
                    this.heart.scale.setTo(0.08, 0.08);
                    game.physics.arcade.enable(this.heart);
                    this.heart.body.velocity.y = -100;
                }
                
            }

            /*if(game.global.score%35==0){
                //if(rand != 6 && rand != 9)
                this.bubble = game.add.sprite(x, 390, 'bubble');
                this.bubble.anchor.setTo(0.5, 1);
                this.bubble.scale.setTo(0.08, 0.08);
                game.physics.arcade.enable(this.bubble);
                this.bubble.body.velocity.y = -100;
                
            }*/

    },

    

};

        // Delete all Phaser initialization code