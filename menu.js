var menuState = {
    create: function() {

    //var database = firebase.database();
    //var username = "";

    
    var nameLabel = game.add.text(game.width/2, game.height/2+50,
        'player: ' + game.global.name, { font: '25px Arial', fill: '#99ccff' });
    nameLabel.anchor.setTo(0.5, 0.5);

    game.global.leaderBoard = [];
    firebase.database().ref('players').orderByChild('score').limitToLast(5).once('value').then(function(snapshot){
        snapshot.forEach(element => {
            //console.log(element.key);
            game.global.leaderBoard.push([element.key,element.val().score]);
        });
    });
    //console.log(game.global.leaderBoard);

    var leaderLabel = game.add.sprite(game.width/2, 150, 'leaderboard');
    leaderLabel.anchor.setTo(0.5,0.5);
    leaderLabel.scale.setTo(0.7,0.7);
    leaderLabel.animations.add('flash', [0, 1], 2, true);
    leaderLabel.play('flash');

    leaderLabel.inputEnabled = true;
    leaderLabel.events.onInputUp.add(function () {
        //this.menuSound.stop();
        game.state.start('leader');
    });

    
    /*var playerScore;
    firebase.database().ref('players/'+game.global.name).once('value').then(function(snapshot){
        console.log(snapshot.val());
        playerScore = snapshot.val().score;
    });
    console.log(playerScore);
    if(playerScore==undefined)
    {
        firebase.database().ref('players/'+game.global.name).set({score: 0});
        game.global.score = 0;
    }
    else game.global.score = playerScore;*/
    

    // Add a background image
    //game.add.image(0, 0, 'background');

    // Display the name of the game
    var gameLabel = game.add.text(game.width/2, 80, 'NS SHAFT',
    { font: '50px Arial', fill: '#ffffff' });
    gameLabel.anchor.setTo(0.5, 0.5);

    // Show the score at the center of the screen
    var scoreLabel = game.add.text(game.width/2, game.height/2,
    'your best: ' + game.global.best, { font: '25px Arial', fill: '#ffdd99' });
    scoreLabel.anchor.setTo(0.5, 0.5);

    // Explain how to start the game
    var startLabel = game.add.text(game.width/2, game.height-80,
    'press the up arrow key to start', { font: '25px Arial', fill: '#ffffff' });
    startLabel.anchor.setTo(0.5, 0.5);

    // Create a new Phaser keyboard variable: the up arrow key
    // When pressed, call the 'start'
    var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    upKey.onDown.add(this.start, this);

    this.menuSound = game.add.audio('menu');
    this.menuSound.volume = 0.3;
    this.menuSound.play();


},
    
    
    start: function() {
    // Start the actual game
    this.menuSound.stop();
    game.state.start('play');
    },
};