# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |**done**|
|:----------------------------------------------------------------------------------------------:|:-----:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |**done**|
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |**done**|
|         All things in your game should have correct physical properties and behaviors.         |  15%  |**done**|
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |**done**|
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |**done**|
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |**done**|
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

# Report
* 在loadState輸入玩家名稱並取得firebase資料
<br>
<img src="load.png" width="700px" height="300px"></img>
<br>
* 顯示玩家名稱與該玩家的最高分
* 點leaderboard按鈕進入排行榜
* 按下鍵盤的上鍵開始遊戲
<br>
<img src="menu.png" width="400px" height="400px"></img>
<br>
* 顯示最高分的五位玩家
* 點選return to menu回到首頁
<br>
<img src="leaderboard.png" width="400px" height="400px"></img>
<br>
* 基本規則
    * 進入遊戲1s後開始
    * 按左右鍵移動
    * 初始血量10，被上方釘子或釘子平台刺到每次-1，歸零時結束遊戲
    * 玩家掉到地圖外會直接結束遊戲
    * 每0.8s隨機生成一個平台，同時分數+1
* 額外工具
    * 平台上的愛心可增加血量
    * 按空白鍵可暫停
    * R鍵回到主選單
    * 額外平台種類：彈簧平台、輸送帶平台、翻轉平台
<br>
<img src="game1.png" width="400px" height="400px"></img>
<br>
<img src="game2.png" width="400px" height="400px"></img>
<br>
<img src="game3.png" width="400px" height="400px"></img>
<br>
<img src="game4.png" width="400px" height="400px"></img>
<br>
* 顯示該回合分數與玩家名稱，若為該玩家最高分會顯示new record
* 按鍵盤上鍵可回到主選單
<br>
<img src="end.png" width="400px" height="400px"></img>
