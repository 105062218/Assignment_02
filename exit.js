var exitState = {
    create: function() {

        // Add a background image
        //game.add.image(0, 0, 'background');
    
        var nameLabel = game.add.text(game.width/2, game.height/2+50,
            'player: ' + game.global.name, { font: '25px Arial', fill: '#99ccff' });
        nameLabel.anchor.setTo(0.5, 0.5);
    
        var playerScore;

        if(game.global.best<game.global.score)
        {
            firebase.database().ref('players/'+game.global.name).set({score: game.global.score});
        }
        firebase.database().ref('players/'+game.global.name+'/score').once('value').then(function(snapshot){
            game.global.best = snapshot.val();
        });
        //console.log(firebase.database().ref('players/'+game.global.name+'/score'));

        // Display the name of the game
        var nameLabel = game.add.text(game.width/2, 80, 'GAME OVER',
        { font: '50px Arial', fill: '#ff0000' });
        nameLabel.anchor.setTo(0.5, 0.5);

        // Show the score at the center of the screen

        var scoreLabel = game.add.text(game.width/2, game.height/2,
            'score: ' + game.global.score, { font: '25px Arial', fill: '#ffdd99' });
        scoreLabel.anchor.setTo(0.5, 0.5);

        if(game.global.best<game.global.score) {
            var label = game.add.sprite(game.width/2, game.height/2-50, 'label');
            label.anchor.setTo(0.5,0.5);
            label.scale.setTo(0.7,0.7);
            label.animations.add('flash', [0, 1], 2, true);
            label.play('flash');
        }

        // Explain how to start the game
        var startLabel = game.add.text(game.width/2, game.height-80,
        'press the up arrow key to return', { font: '25px Arial', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);

        /*var startLabel = game.add.text(game.width/2, game.height-100,
        'press ESC to exit', { font: '25px Arial', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);*/

        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        upKey.onDown.add(this.start, this);

        var exitKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
        exitKey.onDown.add(this.close, this);

        this.dieSound = game.add.audio('die');
        this.dieSound.volume = 0.3;
        this.dieSound.play();
    },

    
    
    start: function() {
    // Start the actual game
        game.state.start('menu');
        this.dieSound.stop();
    },

    close: function(){
        window.close();
    }

};