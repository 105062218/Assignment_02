var loadState = {
    preload: function () {

    // Add a 'loading...' label on the screen
    /*var loadingLabel = game.add.text(game.width/2, 150,
    'loading...', { font: '30px Arial', fill: '#ffffff' });
    loadingLabel.anchor.setTo(0.5, 0.5);*/
    var loadingLabel = game.add.sprite(game.width/2, 150,'loading');
    loadingLabel.anchor.setTo(0.5,0.5);
    //loadingLabel.scale.setTo(0.7,0.7);
    loadingLabel.frame = 0;
    loadingLabel.animations.add('flash', [0, 1, 2], 1.5, true);
    loadingLabel.play('flash');

    // Display the progress bar
    /*var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
    progressBar.anchor.setTo(0.5, 0.5);
    game.load.setPreloadSprite(progressBar);
    */
   
   game.global.name = prompt("Please enter your name", 'player1');
   if(game.global.name == '') game.global.name = 'player1';
   firebase.database().ref('players/'+game.global.name).once('value').then(function(snapshot){
       //console.log(snapshot.val().score);
       game.global.best = snapshot.val().score;
   });
   //console.log(game.global.best);
   if(game.global.best==undefined)
   {
       firebase.database().ref('players/'+game.global.name).set({score: 0});
       game.global.best = 0;
   }

   /*firebase.database().ref('players').orderByChild('score').limitToLast(5).once('value').then(function(snapshot){
        snapshot.forEach(element => {
            console.log(element.key);
            game.global.leaderBoard.push([element.key,element.val().score]);
        });
    });
    console.log(game.global.leaderBoard);*/

    // Load all game assets
    game.load.spritesheet('player', 'assets/player.png',32,32);
    game.load.image('wall', 'assets/wall.png');
    game.load.image('ceiling', 'assets/ceiling.png');
    game.load.image('normal', 'assets/normal.png');
    game.load.image('nails', 'assets/nails.png');
    game.load.spritesheet('conveyor_left', 'assets/conveyor_left.png', 96, 16);
    game.load.spritesheet('conveyor_right', 'assets/conveyor_right.png', 96, 16);
    game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
    game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
    game.load.image('heart', 'assets/heart.png');
    game.load.image('bubble', 'assets/bubble.png');
    game.load.spritesheet('label', 'assets/label.png', 160, 70);
    game.load.spritesheet('leaderboard', 'assets/leaderboard.png', 350, 100);
    game.load.image('pause', 'assets/pause.png');
    game.load.audio('die', ['assets/die.wav', 'assets/die.mp3']);
    game.load.audio('health', ['assets/health.wav', 'assets/health.mp3']);
    game.load.audio('hurt', ['assets/hurt.wav', 'assets/hurt.mp3']);
    game.load.audio('jump', ['assets/jump.wav', 'assets/jump.mp3']);
    game.load.audio('turn', ['assets/turn.wav', 'assets/turn.mp3']);
    game.load.audio('menu', ['assets/menu.wav', 'assets/menu.mp3']);
    game.load.audio('BGsong', ['assets/BGsong.wav', 'assets/BGsong.mp3']);

    // Load a new asset that we will use in the menu state
    //game.load.image('background', 'assets/background.png');
    },

    create: function() {
    // Go to the menu state
    game.time.events.add(Phaser.Timer.SECOND*2, function(){game.state.start('menu');}, this);
    }
}; 