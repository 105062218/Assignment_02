
var game = new Phaser.Game(400, 400, Phaser.AUTO, 'canvas');

// Define our global variable
game.global = { score: 0 , health: 10 , name: "", best: 0, leaderBoard: []};

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('leader', leaderState);
game.state.add('play', playState);
game.state.add('exit', exitState);
game.state.start('boot');