var bootState = {
    preload: function () {
    // Load the progress bar image.
    game.load.spritesheet('loading', 'assets/loading.png', 250, 70);
    
    },

    create: function() {
    // Set some game settings.
    //game.stage.backgroundColor = '#000000';
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.renderer.renderSession.roundPixels = true;
    // Start the load state.
    game.state.start('load');
    }
};